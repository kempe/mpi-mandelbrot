#include "mandel_view.h"
#include <QMouseEvent>

mandel_view::mandel_view(QGraphicsScene* parent) :
    QGraphicsView(parent)
{
}

mandel_view::~mandel_view()
{
}

void mandel_view::mousePressEvent(QMouseEvent* event)
{
    emit mouse_press(event);
}

void mandel_view::mouseMoveEvent(QMouseEvent* event)
{
    emit mouse_move(event);
}
