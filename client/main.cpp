#include "main_window.h"
#include "network_defines.h"
#include <QApplication>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    main_window wnd(PORT, nullptr);
    wnd.show();

    return app.exec();
}
