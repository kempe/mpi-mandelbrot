#ifndef MANDEL_VIEW_H
#define MANDEL_VIEW_H
#include <QGraphicsView>
#include <QGraphicsScene>

class mandel_view : public QGraphicsView
{
    Q_OBJECT
    public:
        mandel_view(QGraphicsScene* parent);
        ~mandel_view();

    protected:
        void mousePressEvent(QMouseEvent* event) override final;
        void mouseMoveEvent(QMouseEvent* event) override final;

    signals:
        void mouse_press(QMouseEvent* event);
        void mouse_move(QMouseEvent* event);
};

#endif
