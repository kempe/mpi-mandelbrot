#include "main_window.h"
#include <QPixmap>
#include <QPushButton>
#include <QDebug>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QScrollBar>
#include <Magick++.h>
#include <string>
#include <complex>

using namespace std;

main_window::main_window(unsigned int port, QWidget* parent) :
    QWidget(parent), socket(nullptr), port(port), drawing_rect(false) 
{
    mandel_image = new QImage;

    QVBoxLayout* layout = new QVBoxLayout(this);

    scene = new QGraphicsScene(this);

    view = new mandel_view(scene);
    view->setInteractive(true);
    connect(view, &mandel_view::mouse_press, this, &main_window::handle_mouse_press);
    connect(view, &mandel_view::mouse_move, this, &main_window::handle_mouse_move);
    view->setMinimumSize(QSize(10, 10));
    view->setMaximumSize(QSize(10, 10));
    

    rect = new QGraphicsRectItem();

    item = new QGraphicsPixmapItem();
    item->setPixmap(QPixmap::fromImage(*mandel_image));

    scene->addItem(item);
    item->setPos(0, 0);

    QPushButton* img_btn = new QPushButton("Show image", this);
    connect(img_btn, &QPushButton::clicked, this, &main_window::show_image);
    layout->addWidget(img_btn);

    QPushButton* rnd_btn = new QPushButton("Render image", this);
    connect(rnd_btn, &QPushButton::clicked, this, &main_window::send_img_render);
    layout->addWidget(rnd_btn);

    QPushButton* exit_btn = new QPushButton("Kill render processes", this);
    connect(exit_btn, &QPushButton::clicked, this, &main_window::handle_kill_renderer);
    layout->addWidget(exit_btn);

    /************ Dialog for entering rendering options *****************************/

    settings_dialog = new QDialog(this);
    QFormLayout* form = new QFormLayout(settings_dialog);
    settings_dialog->setLayout(form);

    QLineEdit* line_edit = new QLineEdit("1", settings_dialog);
    form->addRow("Real max", line_edit);
    field_list.push_back(line_edit);

    line_edit = new QLineEdit("-2", settings_dialog);
    form->addRow("Real min", line_edit);
    field_list.push_back(line_edit);

    line_edit = new QLineEdit("1.4", settings_dialog);
    form->addRow("Imaginary max", line_edit);
    field_list.push_back(line_edit);


    line_edit = new QLineEdit("-1.3", settings_dialog);
    form->addRow("Imaginary min", line_edit);
    field_list.push_back(line_edit);

    line_edit = new QLineEdit("1064", settings_dialog);
    form->addRow("Image width", line_edit);
    field_list.push_back(line_edit);

    line_edit = new QLineEdit("800", settings_dialog);
    form->addRow("Image height", line_edit);
    field_list.push_back(line_edit);

    line_edit = new QLineEdit("1000", settings_dialog);
    form->addRow("Iterations", line_edit);
    field_list.push_back(line_edit);

    QDialogButtonBox* diag_btns = new QDialogButtonBox(QDialogButtonBox::Ok, Qt::Horizontal, 
                                                       settings_dialog);
    form->addRow(diag_btns);

    connect(diag_btns, &QDialogButtonBox::accepted, this, &main_window::handle_update_settings);

    handle_update_settings();


    QPushButton* settings_btn = new QPushButton("Settings", this);
    layout->addWidget(settings_btn);
    connect(settings_btn, &QPushButton::clicked, this, &main_window::show_settings_diag);


    /******************** End dialog ************************************************/

    server = new mandel_tcp_server(this);
    connect(server, &QTcpServer::newConnection, this, &main_window::handle_new_connection);
    server->listen(QHostAddress::Any, port);

    setLayout(layout);
}

main_window::~main_window()
{
    delete mandel_image;
    delete item;
    delete rect;
}

void main_window::show_image()
{
    view->show();
}

void main_window::send_img_render()
{
    if (!socket)
        return;

    // Update the render settings.
    render_settings = new_render_settings;

    QByteArray settings;
    QDataStream stream(&settings, QIODevice::ReadWrite);

    stream << render_settings;

    char* payload = new char[settings.size()];
    QDataStream read_stream(&settings, QIODevice::ReadOnly);
    read_stream.readRawData(payload, settings.size());

    // Send an image request to the server.
    data_packet rnd_packet(op_codes::RENDER_IMG, settings.size(), payload);
    socket->send_data_packet(rnd_packet);
}

void main_window::handle_kill_renderer()
{
    if (!socket)
        return;

    data_packet kill_packet(op_codes::EXIT, 0, nullptr);
    socket->send_data_packet(kill_packet);
}

void main_window::show_settings_diag()
{
    settings_dialog->exec();
}

void main_window::handle_mouse_press(QMouseEvent* event)
{
    if (!drawing_rect)
    {
        scene->addItem(rect);
        rect->setPos(QPointF(event->x(), event->y()));
        rect->setRect(QRectF(0, 0, 1, 1));

        drawing_rect = true;
    }
    else
    {
        scene->removeItem(rect);

        drawing_rect = false;

        // Instantiate a mandelbrot for calculating the new limits for the rendering.
        mandelbrot calculator(render_settings);
        complex<double> upper_left = calculator.get_pixel_coordinate(rect->x(), rect->y());
        complex<double> lower_right = calculator.get_pixel_coordinate(rect->x() + 
                                                                      rect->rect().width(), 
                                                                      rect->y() + 
                                                                      rect->rect().height());

        new_render_settings = render_settings;
        new_render_settings.re_max = real(lower_right);
        new_render_settings.re_min = real(upper_left);
        new_render_settings.im_max = imag(lower_right);
        new_render_settings.im_min = imag(upper_left);

        field_list[0]->setText(QString("%1").arg(new_render_settings.re_max));
        field_list[1]->setText(QString("%1").arg(new_render_settings.re_min));
        field_list[2]->setText(QString("%1").arg(new_render_settings.im_max));
        field_list[3]->setText(QString("%1").arg(new_render_settings.im_min));
    }
}

void main_window::handle_mouse_move(QMouseEvent* event)
{
    if (drawing_rect)
        rect->setRect(QRectF(0, 0, fabs(rect->x() - event->x()), fabs(rect->y() - event->y())));

}

void main_window::handle_new_connection()
{
    socket = server->nextPendingConnection();

    connect(socket, &mandel_socket::disconnected, this, &main_window::handle_socket_close);
    connect(socket, &mandel_socket::packet_ready, this, &main_window::handle_recv_data);

    // Close the server since we only want one connection at a time.
    server->close();
}

void main_window::handle_socket_close()
{
    socket->deleteLater();
    socket = nullptr;

    // If we lose the cluster process we start listening again.
    server->listen(QHostAddress::Any, port);
}

void main_window::handle_recv_data()
{
    data_packet* packet = socket->get_next_packet();

    switch (packet->get_op_code())
    {
        case op_codes::IMG_DATA:
        {
            // Get the image from the packet and display it.
            std::string enc_img(packet->get_payload());

            Magick::Blob blob;
            blob.base64(enc_img);

            mandel_image->loadFromData((const uchar*) blob.data(), blob.length());
            item->setPixmap(QPixmap::fromImage(*mandel_image));
            break;
        }
        default:
            qDebug() << "Unrecognised op code received: " << static_cast<quint32>(packet->get_op_code());
    }
    
    delete packet;
}

void main_window::handle_update_settings()
{
    settings_dialog->hide();

    render_settings.re_max = field_list[0]->text().toDouble();
    render_settings.re_min = field_list[1]->text().toDouble();
    render_settings.im_max = field_list[2]->text().toDouble();
    render_settings.im_min = field_list[3]->text().toDouble();
    render_settings.img_width = field_list[4]->text().toULong();
    render_settings.img_height = field_list[5]->text().toULong();
    render_settings.iterations = field_list[6]->text().toULong();

    new_render_settings = render_settings;

    delete mandel_image;

    scene->setSceneRect(0, 0, render_settings.img_width, render_settings.img_height);
    view->setMinimumSize(render_settings.img_width, render_settings.img_height);
    view->setMaximumSize(render_settings.img_width, render_settings.img_height);
    view->verticalScrollBar()->hide();
    view->horizontalScrollBar()->hide();
    mandel_image = new QImage(render_settings.img_width, render_settings.img_height, QImage::Format_RGB32);
}
