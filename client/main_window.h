#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#include <QWidget>
#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QLineEdit>
#include <QDialog>
#include <QMouseEvent>
#include <vector>
#include "mandel_tcp_server.h"
#include "mandel_socket.h"
#include "mandelbrot.h"
#include "mandel_view.h"

class main_window : public QWidget
{
    Q_OBJECT
    public:
        main_window(unsigned int port, QWidget* parent);
        ~main_window();

    private:
        QImage* mandel_image;

        QGraphicsScene* scene;
        mandel_view* view;
        QGraphicsPixmapItem* item;

        mandel_tcp_server* server;
        mandel_socket* socket;

        const unsigned int port;

        QDialog* settings_dialog;
        // Contains user set settings.
        std::vector<QLineEdit*> field_list;
        mandel_settings render_settings;
        mandel_settings new_render_settings;

        bool drawing_rect;
        QGraphicsRectItem* rect;

    public slots:
        void show_image();
        void send_img_render();
        void handle_kill_renderer();
        void show_settings_diag();
        void handle_mouse_press(QMouseEvent* event);
        void handle_mouse_move(QMouseEvent* event);

    private slots:
        void handle_new_connection();
        void handle_socket_close();
        void handle_recv_data();
        void handle_update_settings();

};

#endif
