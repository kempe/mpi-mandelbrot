#ifndef MANDELBROT_H
#define MANDELBROT_H
#include <complex>
#include <QDataStream>

struct mandel_settings
{
    double re_max;
    double re_min;

    double im_max;
    double im_min;

    unsigned long img_width;
    unsigned long img_height;

    unsigned long iterations;
};
QDataStream& operator<<(QDataStream& stream, const mandel_settings& settings);
QDataStream& operator>>(QDataStream& stream, mandel_settings& settings);


class mandelbrot
{
    public:
        mandelbrot(mandel_settings settings);
        ~mandelbrot();

        std::complex<double> get_pixel_coordinate(int x, int y) const;

        unsigned long is_mandelbrot_point(unsigned long x, unsigned long y, unsigned long iterations) const;

        void calculate_set();

        size_t get_number_of_points() const;
        const unsigned long* get_point_array() const;

        // Deleted contructors.
        mandelbrot(const mandelbrot&) = delete;
        mandelbrot(mandelbrot&&) = delete;
        mandelbrot& operator=(const mandelbrot&) = delete;
        mandelbrot& operator=(mandelbrot&&) = delete;

    private:
        double re_max;
        double re_min;

        double im_max;
        double im_min;

        unsigned long img_width;
        unsigned long img_height;

        unsigned long iterations;

        unsigned long* calculated_points;
};

// Helper functions for usage of the class with MPI.
double get_min_im(double im_max, double im_min, int id, int procs);
double get_max_im(double im_max, double im_min, int id, int procs);

#endif
