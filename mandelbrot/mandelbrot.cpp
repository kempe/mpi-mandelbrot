#include "mandelbrot.h"
#include <stdexcept>
#include <iostream>

using namespace std;

mandelbrot::mandelbrot(mandel_settings settings)
{
    re_max = settings.re_max;
    re_min = settings.re_min;

    im_max = settings.im_max;
    im_min = settings.im_min;

    img_width = settings.img_width;
    img_height = settings.img_height;

    iterations = settings.iterations;

    calculated_points = new unsigned long[img_width * img_height];
}

mandelbrot::~mandelbrot()
{
    delete[] calculated_points;
}

complex<double> mandelbrot::get_pixel_coordinate(int x, int y) const
{
    double re_size = fabs(re_max - re_min);
    double im_size = fabs(im_max - im_min);


    double re_pp = re_size / img_width;
    double im_pp = im_size / img_height;

    return complex<double>(re_pp * x + re_min, im_pp * y + im_min);
}

unsigned long mandelbrot::is_mandelbrot_point(unsigned long x, unsigned long y, unsigned long iterations) const
{
    complex<double> point = get_pixel_coordinate(x, y);
    complex<double> series_value(0, 0);
    
    for (unsigned long i = 0; i < iterations; ++i)
    {
        if (real(series_value) * real(series_value) + imag(series_value) * imag(series_value) >= 4)
            return i + 1 - log(log(abs(pow(series_value, 2) + point))) / log(2);

        series_value = pow(series_value, 2) + point;
    }

    return iterations;
}

void mandelbrot::calculate_set()
{
    for (unsigned long y = 0; y < img_height; ++y)
    {
        for (unsigned long x = 0; x < img_width; ++x)
        {
            unsigned long result = is_mandelbrot_point(x, y, iterations);

            calculated_points[y * img_width + x] = result;
        }
    }

}

size_t mandelbrot::get_number_of_points() const 
{
    return img_height * img_width;
}

const unsigned long* mandelbrot::get_point_array() const 
{
    return calculated_points;
}

double get_min_im(double im_max, double im_min, int id, int procs)
{
    return im_min + ((fabs(im_max - im_min)) / procs) * id;
}


double get_max_im(double im_max, double im_min, int id, int procs)
{
    return im_min + ((fabs(im_max - im_min)) / procs) * (id + 1);
}

QDataStream& operator<<(QDataStream& stream, const mandel_settings& settings)
{
    stream << settings.re_max;
    stream << settings.re_min;
    stream << settings.im_max;
    stream << settings.im_min;
    stream << static_cast<quint64>(settings.img_width);
    stream << static_cast<quint64>(settings.img_height);
    stream << static_cast<quint64>(settings.iterations);

    return stream;
}

QDataStream& operator>>(QDataStream& stream, mandel_settings& settings)
{
    double re_max, re_min, im_max, im_min;
    quint64 img_width, img_height, iterations;

    stream >> re_max >> re_min >> im_max >> im_min
           >> img_width >> img_height >> iterations;

    settings.re_max = re_max;
    settings.re_min = re_min;
    settings.im_max = im_max;
    settings.im_min = im_min;
    settings.img_width = img_width;
    settings.img_height = img_height;
    settings.iterations = iterations;

    return stream;
}
