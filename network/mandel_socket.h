#ifndef MANDEL_SOCKET_H
#define MANDEL_SOCKET_H
#include "data_packet.h"
#include <QTcpSocket>
#include <queue>


class mandel_socket : public QTcpSocket
{
    Q_OBJECT
    public:
        mandel_socket(QObject* parent = nullptr);
        mandel_socket(QString host, unsigned int port, QObject* parent);
        ~mandel_socket();

        void send_data_packet(const data_packet& packet);
        data_packet* get_next_packet();

        bool is_transmission_done();

    private:
        void setup_slot_connection();

        QByteArray recv_buf;
        std::queue<data_packet*> packet_queue;

        void handle_op_code(const QByteArray& data);
        void decode_packet_data(QByteArray& incoming);

        bool transmission_done;
        qint64 bytes_written;
        qint64 bytes_to_write;

    private slots:
        void handle_recv_data();
        void handle_bytes_written(qint64 written_bytes);

    signals:
        void packet_ready();
};

#endif
