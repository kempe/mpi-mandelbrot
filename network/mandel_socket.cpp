#include "mandel_socket.h"
#include "data_packet.h"
#include <QDebug>
#include <QDataStream>
#include <QByteArray>

using namespace std;

mandel_socket::mandel_socket(QString host, unsigned int port, QObject* parent) :
    QTcpSocket(parent), transmission_done(false), bytes_written(0), bytes_to_write(0)
{
    setup_slot_connection();
    connectToHost(host, port, QIODevice::ReadWrite, QAbstractSocket::AnyIPProtocol);
}

mandel_socket::mandel_socket(QObject* parent) : 
    QTcpSocket(parent), transmission_done(false), bytes_written(0), bytes_to_write(0)
{
    setup_slot_connection();
}

mandel_socket::~mandel_socket()
{
    while (!packet_queue.empty())
    {
        delete packet_queue.front();
        packet_queue.pop();
    }
}

void mandel_socket::send_data_packet(const data_packet& packet)
{
    QByteArray buf;
    QDataStream stream(&buf, QIODevice::ReadWrite);

    stream << packet;

    write(buf);

    transmission_done = false;
    bytes_written = 0;
    bytes_to_write = buf.size();
}

data_packet* mandel_socket::get_next_packet()
{
    if (packet_queue.empty())
        return nullptr;

    data_packet* tmp = packet_queue.front();
    packet_queue.pop();

    return tmp;
}

bool mandel_socket::is_transmission_done()
{
    return transmission_done;
}

void mandel_socket::setup_slot_connection()
{
    connect(this, &mandel_socket::readyRead, this, &mandel_socket::handle_recv_data);
    connect(this, &mandel_socket::bytesWritten, this, &mandel_socket::handle_bytes_written);
}

void mandel_socket::decode_packet_data(QByteArray& incoming)
{
    quint32 op_code = 0;
    uint payload_size = 0;

    recv_buf.append(incoming);

    if (recv_buf.size() >= packet_header_size)
    {
        QDataStream reader(&recv_buf, QIODevice::ReadOnly);

        reader >> op_code >> payload_size;

        if (recv_buf.size() < payload_size + packet_header_size)
            return;

        char* payload = nullptr;
        if (payload_size > 0)
        {
            payload = new char[payload_size];

            reader.readRawData(payload, payload_size);
        }

        data_packet* reassembled = new data_packet(op_code, payload_size, payload);
        packet_queue.push(reassembled);
        emit(packet_ready());

        // Remove the read data from the buffer.
        recv_buf.remove(0, payload_size + packet_header_size);

        // If we still have data in the buffer we call ourselves to handle it.
//        if (recv_buf.size() > 0)
//            decode_packet_data(recv_buf);
    }

}

void mandel_socket::handle_recv_data()
{
    QByteArray incoming;

    incoming = readAll();

    decode_packet_data(incoming);

}

void mandel_socket::handle_bytes_written(qint64 written_bytes)
{
    bytes_written += written_bytes;

    if (bytes_written >= bytes_to_write)
        transmission_done = true;
}
