#include "data_packet.h"

data_packet::data_packet(op_codes op_code, uint payload_size, char* payload) :
    op_code(static_cast<quint32>(op_code)), payload_size(payload_size), payload(payload)
{
}

data_packet::data_packet(quint32 op_code, uint payload_size, char* payload) :
    op_code(op_code), payload_size(payload_size), payload(payload)
{
}

data_packet::~data_packet()
{
    delete[] payload;
    payload = nullptr;
}

op_codes data_packet::get_op_code() const
{
    return static_cast<op_codes>(op_code);
}

uint data_packet::get_payload_size() const
{
    return payload_size;
}

const char* data_packet::get_payload() const
{
    return payload;
}

QDataStream& operator<<(QDataStream& stream, const data_packet& packet)
{
    stream << packet.op_code;
    stream << packet.payload_size;
    stream.writeRawData(packet.payload, packet.payload_size);

    return stream;
}
