#include "mandel_tcp_server.h"

mandel_tcp_server::mandel_tcp_server(QObject* parent) :
    QTcpServer(parent)
{
}

mandel_socket* mandel_tcp_server::nextPendingConnection()
{
    mandel_socket* tmp;

    if (socket_stack.empty())
        return nullptr;

    tmp = socket_stack.front();
    socket_stack.pop();

    return tmp;
}

void mandel_tcp_server::incomingConnection(qintptr socketDescriptor)
{
    mandel_socket* socket = new mandel_socket(this);

    socket->setSocketDescriptor(socketDescriptor);
    socket_stack.push(socket);
}
    
