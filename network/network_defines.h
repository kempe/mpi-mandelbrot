#ifndef NETWORK_DEFINES_H
#define NETWORK_DEFINES_H
#include <Qt>

#define PORT 8910

enum op_codes : quint32
{
    /* Used to send image data to the client.
     * The image should be in the payload
     * encoded as base64 using ImageMagicks
     * Blob-class. */
    IMG_DATA = 0x1,
    RENDER_IMG = 0x2,
    EXIT = 0x3
};

#endif
