#ifndef MANDEL_TCP_SERVER_H
#define MANDEL_TCP_SERVER_H
#include "mandel_socket.h"
#include <QTcpServer>
#include <queue>

class mandel_tcp_server : public QTcpServer
{
    public:
        mandel_tcp_server(QObject* parent);
        mandel_socket* nextPendingConnection() override final;
        void incomingConnection(qintptr socketDescriptor) override final;

    private:
        std::queue<mandel_socket*> socket_stack;
};

#endif
