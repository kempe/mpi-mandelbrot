#ifndef DATA_PACKET_H
#define DATA_PACKET_H
#include "network_defines.h"
#include <QDataStream>

static unsigned short packet_header_size = 8;

class data_packet
{
    friend QDataStream& operator<<(QDataStream&, const data_packet&);

    public:
        // The user is responsible for allocating memory for payload.
        // data_packet will then call delete on it in its constructor.
        data_packet(op_codes op_code, uint payload_size, char* payload);
        data_packet(quint32 op_code, uint payload_size, char* payload);
        ~data_packet();

        op_codes get_op_code() const;
        uint get_payload_size() const;
        const char* get_payload() const;

    private:
        quint32 op_code;
        uint payload_size;

        char* payload;
};

QDataStream& operator<<(QDataStream& stream, const data_packet& packet);

#endif
