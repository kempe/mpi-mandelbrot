#include "mandel_render_srv.h"
#include <mpi.h>
#include <QCoreApplication>
#include <string>
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    int id;
    int procs;


    // Init MPI.
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    // For the Qt event loop.
    QCoreApplication app(argc, argv);

    if (argc != 2)
    {
        cout << "Usage: mpi-mandel-srv client-address" << endl;
        return 1;
    }

    string host(argv[1]);
    mandel_render_srv srv(host, PORT, id, procs, nullptr);

    int exit_code = app.exec();
    MPI_Finalize();

    return exit_code;
}
