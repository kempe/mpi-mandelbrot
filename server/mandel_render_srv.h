#ifndef MANDEL_RENDER_SRV_H
#define MANDEL_RENDER_SRV_H
#include <QObject>
#include "mandel_socket.h"
#include "mandelbrot.h"

enum class mpi_opcodes : int 
{
    EXIT = 0,
    RENDER = 1
};

class mandel_render_srv : public QObject
{
    public:
        mandel_render_srv(std::string host, unsigned int port, 
                          int id, int procs, QObject* parent);

        void start_drawing();

    private:
        void root_process_handling(unsigned long *receive_buffer);
        void mpi_event_loop();

        mandel_settings settings;
        const int id;
        const int procs;

        mandel_socket* socket;

    private slots:
            void handle_recv_data();
            void handle_network_down();
            void handle_socket_error(QAbstractSocket::SocketError error);
};

#endif
