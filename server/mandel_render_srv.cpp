#include "mandel_render_srv.h"
#include <iostream>
#include <mpi.h>
#include <Magick++.h>
#include <iomanip>
#include <cmath>
#include <complex>

using namespace std;
using namespace Magick;

mandel_render_srv::mandel_render_srv(std::string host, unsigned int port,
                                     int id, int procs, QObject* parent) :
    QObject(parent), id(id), procs(procs)
{
    // We only let the root process connect to the client.
    if (id == 0)
    {
        cout << "Starting root process!" << endl;
        socket = new mandel_socket(host.c_str(), port, this);
        connect(socket, &mandel_socket::packet_ready, this, &mandel_render_srv::handle_recv_data);
        connect(socket, &mandel_socket::disconnected, this, &mandel_render_srv::handle_network_down);
        connect(socket, static_cast<void (mandel_socket::*)(QAbstractSocket::SocketError)>(&mandel_socket::error), 
                this, &mandel_render_srv::handle_socket_error);
    }
    else
    {
        cout << "Starting process " << id << "!" << endl;
        socket = nullptr;
        mpi_event_loop();
    }
}

void mandel_render_srv::start_drawing()
{
    unsigned long *receive_buffer = nullptr;

    mandelbrot mandel(settings);
    mandel.calculate_set();

    if (id == 0)
        receive_buffer = new unsigned long[settings.img_width * settings.img_height * procs];

    cout << "Process: " << id <<  ", gathering!" << endl;
    MPI_Gather(mandel.get_point_array(), settings.img_width * settings.img_height, 
               MPI_UNSIGNED_LONG, receive_buffer, settings.img_width * settings.img_height, 
               MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);

    cout << "Process: " << id << ", done gathering!" << endl;

    if (id == 0)
        root_process_handling(receive_buffer);

    delete[] receive_buffer;

}

void mandel_render_srv::root_process_handling(unsigned long *receive_buffer)
{
    Image my_image(Geometry(settings.img_width, settings.img_height * procs), "white");
    my_image.modifyImage();

    Pixels pixel_cache(my_image);
    PixelPacket *pixels = pixel_cache.get(0, 0, settings.img_width, settings.img_height * procs);

    cout << "Assembling image!" << endl;
    for (unsigned long y = 0; y < settings.img_height * procs; ++y)
    {
        cout << "Progress: " << setprecision(3) << 
            static_cast<float>(y * 100) / (settings.img_height * procs) << " %            \r" << flush;

        for (unsigned long x = 0; x < settings.img_width; ++x)
        {
            const unsigned long& iter_res = receive_buffer[y * settings.img_width + x];
            if (iter_res == settings.iterations)
                pixels[y * settings.img_width + x] = Color(MaxRGB * 0.55, MaxRGB * 0.35, MaxRGB * 0.6);
            else
                pixels[y * settings.img_width + x] = Color(MaxRGB * cosf(exp(iter_res)), 0, 
                                                           MaxRGB * sinf(exp(iter_res)));
        }
    }
    pixel_cache.sync();
    cout << "Progress: 100 %              " << endl;

    cout << "Sending image!" << endl;

    Blob image_data;
    my_image.magick("PNG");
    my_image.write(&image_data);

    std::string enc_img = image_data.base64();
    char* data = new char[enc_img.size() + 1];
    memcpy(data, enc_img.c_str(), enc_img.size());
    data[enc_img.size()] = '\0';

    data_packet packet(op_codes::IMG_DATA, enc_img.size() + 1, data);
    socket->send_data_packet(packet);
}

void mandel_render_srv::mpi_event_loop()
{
    int recv_op_code;
    while (true)
    {
        MPI_Status status;

        // Sending the opcode as a double as well to only have to use one transmission.
        MPI_Recv(&recv_op_code, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

        mpi_opcodes op_code = static_cast<mpi_opcodes>(recv_op_code);
        switch (op_code)
        {
            case mpi_opcodes::EXIT:
            {
                MPI_Finalize();
                exit(0);
                break;
            }
            case mpi_opcodes::RENDER:
            {
                qDebug() << "Starting rendering!";
                double mpi_data[4];
                unsigned long mpi_data1[3];
                MPI_Recv(&mpi_data, 4, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
                MPI_Recv(&mpi_data1, 3, MPI_UNSIGNED_LONG, 0, 0, MPI_COMM_WORLD, &status);
                mandel_settings recv_settings = { .re_max = mpi_data[0], 
                                                  .re_min = mpi_data[1],
                                                  .im_max = get_max_im(mpi_data[2], mpi_data[3], id, procs),
                                                  .im_min = get_min_im(mpi_data[2], mpi_data[3], id, procs),
                                                  .img_width = mpi_data1[0],
                                                  .img_height = mpi_data1[1],
                                                  .iterations = mpi_data1[2] };

                settings = recv_settings;

                start_drawing();
                break;
            }
        }
    }
}

void mandel_render_srv::handle_recv_data()
{
    data_packet* packet = socket->get_next_packet();

    switch (packet->get_op_code())
    {
        case op_codes::RENDER_IMG:
        {
            cout << "Dispatching and starting image rendering!" << endl;
            QByteArray payload;
            QDataStream stream(&payload, QIODevice::ReadWrite);

            stream.writeRawData(packet->get_payload(), packet->get_payload_size());

            mandel_settings recv_settings;
            QDataStream read_stream(&payload, QIODevice::ReadOnly);
            read_stream >> recv_settings;

            settings = recv_settings;

            settings.img_height /= procs;

            double mpi_data[4];

            mpi_data[0] = settings.re_max;
            mpi_data[1] = settings.re_min;
            mpi_data[2] = settings.im_max;
            mpi_data[3] = settings.im_min;

            unsigned long mpi_data1[3];
            mpi_data1[0] = settings.img_width;
            mpi_data1[1] = settings.img_height;
            mpi_data1[2] = settings.iterations;

            // Send current render settings to other processes.
            if (procs >= 2)
            {
                for (int i = 1; i < procs; ++i)
                {
                    // Send op code
                    int op_code = static_cast<int>(mpi_opcodes::RENDER); 
                    MPI_Send(&op_code, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                    MPI_Send(mpi_data, 4, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
                    MPI_Send(mpi_data1, 3, MPI_UNSIGNED_LONG, i, 0, MPI_COMM_WORLD);
                }
            }

            // Update the settings for the root process.
            double im_max = settings.im_max;
            double im_min = settings.im_min;
            settings.im_max = get_max_im(im_max, im_min, 0, procs);
            settings.im_min = get_min_im(im_max, im_min, 0, procs);

            start_drawing();
            break;
        }
        case op_codes::EXIT:
        {
            int mpi_data = 0;
            // Send exit command.
            if (procs >= 2)
            {
                for (int i = 1; i < procs; ++i)
                    MPI_Send(&mpi_data, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            }

            MPI_Finalize();
            exit(0);
            break;
        }
        default:
            qDebug() << "Unrecognised op code received: " << static_cast<quint32>(packet->get_op_code());
    }
    
    delete packet;
}

void mandel_render_srv::handle_network_down()
{
    cout << "The network connection died! Shutting down!" << endl;
    exit(1);
}

void mandel_render_srv::handle_socket_error(QAbstractSocket::SocketError error)
{
    cout << "Socket error: " << socket->errorString().toStdString() << ", Shutting down!" << endl;
    exit(1);
}
